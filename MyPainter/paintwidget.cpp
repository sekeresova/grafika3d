#include "paintwidget.h"


PaintWidget::PaintWidget(Ui::MyPainterClass *parentUi, QWidget *parent)
	: QWidget(parent)
{
	ui = parentUi;
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::black;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::DDA(int x1, int y1, int x2, int y2, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(QColor(farba), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	float  m, xx, yy, deltaY, deltaX, X, Y;
	deltaX = x2 - x1;
	deltaY = y2 - y1;
	if (fabs(deltaY) <= fabs(deltaX)) {
		m = (deltaY / deltaX);
		if (x1 < x2) { xx = x1; yy = y1; X = x2; }
		else { xx = x2; yy = y2; X = x1; }
		painter.drawEllipse((int)xx, (int)yy, 1, 1);
		while (xx < X) {
			xx = xx + 1.0;
			yy = yy + m;
			painter.drawEllipse(xx, (int)yy, 1, 1);
		}
	}
	else {
		m = (deltaX / deltaY);
		if (y1 < y2) { xx = x1; yy = y1; Y = y2; }
		else { xx = x2; yy = y2; Y = y1; }
		painter.drawEllipse((int)xx, (int)yy, 1, 1);
		while (yy < Y) {
			xx = xx + m;
			yy = yy + 1.0;
			painter.drawEllipse((int)xx, yy, 1, 1);
		}
	}
	update();
}

void PaintWidget::spracovanieSuboru(QTextStream * text)
{
	QPainter painter(&image);
	QString riadok;
	BOD bod;
	TROJUHOLNIK troj;
	bodElipsy.clear();
	bodTrojuholnika.clear();
	xs = image.width() / 2;
	ys = image.height() / 2;

	text->readLine();
	text->readLine();
	text->readLine();
	text->readLine();

	pocetBodov = text->readLine().split(' ')[1].toInt();

	for (int i = 0; i < pocetBodov; i++) {
		riadok = text->readLine();
		QStringList rozdelenyRiadok = riadok.split(' ');
		bod.x = rozdelenyRiadok[0].toDouble();
		bod.y = rozdelenyRiadok[1].toDouble();
		bod.z = rozdelenyRiadok[2].toDouble();
		bodElipsy.push_back(bod);
	}

	int pocetTrojuholnikov = text->readLine().split(' ')[1].toInt();

	for (int i = 0; i < pocetTrojuholnikov; i++) {
		riadok = text->readLine();
		QStringList rozdelenyRiadok = riadok.split(' ');
		troj.a = rozdelenyRiadok[1].toInt();
		troj.b = rozdelenyRiadok[2].toInt();
		troj.c = rozdelenyRiadok[3].toInt();
		bodTrojuholnika.push_back(troj);
	}
	bodUpraveny = bodElipsy;
}

void PaintWidget::rovnobeznePremietanie()
{
	bodRovnobezPremiet = vykreslenie(bodElipsy);
	//bodUpraveny = bodRovnobezPremiet;
}

void PaintWidget::stredovePremietanie()
{
	vector<BOD> stredovyPremietBod;
	BOD stredoveBod;
	int index0, index1, index2;
	float vzdialenost = ui->doubleSpinBoxVzdialenostKamery->value();
	for (int i = 0; i < bodElipsy.size(); i++) {
		double scaleInverse = 1.0 - vzdialenost;
		int x = float(float(bodElipsy[i].x * vzdialenost)) + (xs - scaleInverse);
		int y = float(float(bodElipsy[i].y * vzdialenost)) + (ys - scaleInverse);
		stredoveBod.x = x - xs;
		stredoveBod.y = y - ys;
		stredovyPremietBod.push_back(stredoveBod);
	}
	bodStredovyPriemet = vykreslenie(stredovyPremietBod);
}

void PaintWidget::rotaciaOkoloZ(QColor Il, QColor rs, QColor rd, QColor Io, QColor ra)
{
	double posunutieX = ui->doubleSpinBoxOsX->value();
	double posunutieY = ui->doubleSpinBoxOsY->value();
	BOD bodZrotovanyPomocny;
	vector<BOD> bodZrotovany;
	double rozsah = 360.0;
	double polohaZenit = ui->horizontalSliderZenit->value();
	double uhol = (polohaZenit / rozsah);
	for (int i = 0; i < bodElipsy.size(); i++) {
		double x = round((double((bodUpraveny[i].x - posunutieX) * cos(uhol)) - double((bodUpraveny[i].y - posunutieY) * sin(uhol))) + posunutieX);
		double y = round((double((bodUpraveny[i].x - posunutieX) * sin(uhol)) + double((bodUpraveny[i].y - posunutieY) * cos(uhol))) + posunutieY);
		bodZrotovanyPomocny.x = x;
		bodZrotovanyPomocny.y = y;
		bodZrotovanyPomocny.z = bodUpraveny[i].z;
		bodZrotovany.push_back(bodZrotovanyPomocny);
	}
	bodUpraveny = bodZrotovany;
	bodZrotovanyPosunuty = vykreslenie(bodUpraveny);
	
	
//	printf("A\n");
//	system("PAUSE\n");
	QColor farba;
	for (int i = 0; i < bodTrojuholnika.size(); i++) {
		printf("\n%d\n",i);
		bodNormaly.clear();
		pomBodTroj.clear();
		farba = PhongovOsvetlovaciModel(bodZrotovanyPosunuty, bodTrojuholnika[i], Il, rs, rd, Io, ra);
		rozdelTrojuholnik(bodTrojuholnika[i], farba, bodZrotovanyPosunuty);
	//	printf("E\n");
	//	system("PAUSE\n");
	}
	printf("F\n");
//	system("PAUSE\n");
}

void PaintWidget::rotaciaOkoloX(QColor Il, QColor rs, QColor rd, QColor Io, QColor ra)
{
	bodZrotovanyPosunuty.clear();
	double posunutieX = ui->doubleSpinBoxOsX->value();
	double posunutieY = ui->doubleSpinBoxOsY->value();
	double posunutieZ = ui->doubleSpinBoxOsZ->value();
	BOD bodZrotovanyPomocny;
	vector<BOD> bodZrotovany;
	bodZrotovany.clear();

	double rozsah = 360.0;
	double polohaAzimut = ui->horizontalSliderAzimut->value();
	double uhol = (polohaAzimut / rozsah);
	for (int i = 0; i < bodElipsy.size(); i++) {
		double y = round((double((bodUpraveny[i].y - posunutieY) * cos(uhol)) - double((bodUpraveny[i].z - posunutieZ) * sin(uhol)))+ posunutieY);
		double z = round((double((bodUpraveny[i].y - posunutieY) * sin(uhol)) + double((bodUpraveny[i].z - posunutieZ) * cos(uhol))) + posunutieZ);
		bodZrotovanyPomocny.x = bodUpraveny[i].x;
		bodZrotovanyPomocny.y = y;
		bodZrotovanyPomocny.z = z;
		bodZrotovany.push_back(bodZrotovanyPomocny);
	}
	bodUpraveny = bodZrotovany;
	bodZrotovanyPosunuty = vykreslenie(bodUpraveny);

	//clearImage();
	printf("A\n");
	//system("PAUSE\n");
	QColor farba;
	for (int i = 0; i < bodTrojuholnika.size(); i++) {
		printf("\n%d\n", i);
		bodNormaly.clear();
		pomBodTroj.clear();
		farba = PhongovOsvetlovaciModel(bodZrotovanyPosunuty, bodTrojuholnika[i], Il, rs, rd, Io, ra);
		rozdelTrojuholnik(bodTrojuholnika[i], farba, bodZrotovanyPosunuty);
		printf("E\n");
	//	system("PAUSE\n");
	}
	printf("F\n");
//	system("PAUSE\n");

}

vector<BOD> PaintWidget::vykreslenie(vector<BOD> upraveneBody)
{
	vector<BOD> bodVykresleny(pocetBodov);
	QPainter painter(&image);
	int index0, index1, index2;
	float bod0, bod1, bod2;
	BOD bodA, bodB, bodC;
	for (int i = 0; i < bodTrojuholnika.size(); i++) {
		index0 = bodTrojuholnika[i].a;
		index1 = bodTrojuholnika[i].b;
		index2 = bodTrojuholnika[i].c;
		//ulozim posunute (do stredu scrollArea, kresliacieho platna) body na scanLine algoritmus
		bodA.x = upraveneBody[index0].x + xs;
		bodA.y = upraveneBody[index0].y + ys;
		bodA.z = upraveneBody[index0].z;
		bodB.x = upraveneBody[index1].x + xs;
		bodB.y = upraveneBody[index1].y + ys;
		bodB.z = upraveneBody[index1].z;
		bodC.x = upraveneBody[index2].x + xs;
		bodC.y = upraveneBody[index2].y + ys;
		bodC.z = upraveneBody[index2].z;
		painter.drawLine(bodA.x, bodA.y, bodB.x, bodB.y);
		painter.drawLine(bodB.x, bodB.y, bodC.x, bodC.y);
		painter.drawLine(bodC.x, bodC.y, bodA.x, bodA.y);

		bodVykresleny[index0] = bodA;
		bodVykresleny[index1] = bodB;
		bodVykresleny[index2] = bodC;
	}
	return bodVykresleny;
	update();
}

void PaintWidget::posunutie()
{
	vector<BOD> bodPosunuty;
	BOD bod;
	for (int i = 0; i < bodElipsy.size(); i++) {
		bod.x = round(bodElipsy[i].x + ui->doubleSpinBoxOsX->value());
		bod.y = round(bodElipsy[i].y + ui->doubleSpinBoxOsY->value());
		bod.z = round(bodElipsy[i].z + ui->doubleSpinBoxOsZ->value());
		bodPosunuty.push_back(bod);
	}
	vykreslenie(bodPosunuty);
	bodUpraveny = bodPosunuty;
}

void PaintWidget::skalovanie()
{
	vector<BOD> bodSkalovany;
	BOD bod;
	double deltaX, deltaY, deltaZ;
	for (int i = 0; i < bodElipsy.size(); i++) {
		deltaX = bodElipsy[i].x - bodElipsy[0].x;
		deltaY = bodElipsy[i].y - bodElipsy[0].y;
		deltaZ = bodElipsy[i].z - bodElipsy[0].z;
		bod.x = round(ui->doubleSpinBoxSkalovanie->value() * deltaX + bodElipsy[0].x);
		bod.y = round(ui->doubleSpinBoxSkalovanie->value() * deltaY + bodElipsy[0].y);
		bod.z = round(ui->doubleSpinBoxSkalovanie->value() * deltaZ + bodElipsy[0].z);
		bodSkalovany.push_back(bod);
	}
	vykreslenie(bodSkalovany);
	bodUpraveny = bodSkalovany;
}

BOD PaintWidget::normovanieBodu(BOD bod)
{
	BOD normaBod;
	float dlzka;

	dlzka = sqrt(pow(bod.x, 2) + pow(bod.y, 2) + pow(bod.z, 2));
	normaBod.x = bod.x / dlzka;
	normaBod.y = bod.y / dlzka;
	normaBod.z = bod.z / dlzka;

	return normaBod;
}

QColor PaintWidget::PhongovOsvetlovaciModel(vector<BOD> body, TROJUHOLNIK troj, QColor Il, QColor rs, QColor rd, QColor Io, QColor ra)
{
	printf("som vo PHONGOVOM OSVETLOVACOM MODELE\n");

	int indexA, indexB, indexC, A, B, C;
	double LkratN, dlzkaodrazenehoLuca, dlzkaNormaly, dlzkaSvetelnehoLuca, dlzkaVektoraPohladu, VkratR, pomRed, pomGreen, pomBlue, LkratNnormovane;
	BOD u, v, normala, svetelnyLucPozicia, odrazenyLuc, vektorPohladu;
	QColor Is, Ia, Id, I;
	//rovnobezne premietanie

		//INICIALIZACIA NORMALY
		indexA = troj.a;
		indexB = troj.b;
		indexC = troj.c;
	/*	printf("indexA: %d\n", indexA);
		printf("indexB: %d\n", indexB);
		printf("indexC: %d\n", indexC);*/
		//u = B - A
		u.x = body[indexB].x - body[indexA].x;
		u.y = body[indexB].y - body[indexA].y;
		u.z = body[indexB].z - body[indexA].z;
	/*	printf("u.x :%lf\n", u.x);
		printf("u.y :%lf\n", u.y);
		printf("u.z :%lf\n", u.z);*/
		//v = B - C
		v.x = body[indexB].x - body[indexC].x;
		v.y = body[indexB].y - body[indexC].y;
		v.z = body[indexB].z - body[indexC].z;
	/*	printf("v.x :%lf\n", v.x);
		printf("v.y :%lf\n", v.y);
		printf("v.z :%lf\n", v.z);*/
		//normala
		normala.x = u.y * v.z - u.z * v.y;
		normala.y = u.z * v.x - u.x * v.z;
		normala.z = u.x * v.y - u.y * v.x;
		normala = normovanieBodu(normala);

		//INICIALIZACIA SVETELNEHO LUCA
		svetelnyLucPozicia.x = 10;
		svetelnyLucPozicia.y = 10;
		svetelnyLucPozicia.z = 20;
		svetelnyLucPozicia = normovanieBodu(svetelnyLucPozicia);
		//INICIALIZACIA ODRAZENEHO LUCA
		// R = 2(L.N)N - L
		// L.N = skalarny sucin
		LkratN = (svetelnyLucPozicia.x * normala.x) + (svetelnyLucPozicia.y * normala.y) + (svetelnyLucPozicia.z * normala.z);
		odrazenyLuc.x = 2 * (LkratN)* normala.x - svetelnyLucPozicia.x;
		odrazenyLuc.y = 2 * (LkratN)* normala.y - svetelnyLucPozicia.y;
		odrazenyLuc.z = 2 * (LkratN)* normala.z - svetelnyLucPozicia.z;
		odrazenyLuc = normovanieBodu(odrazenyLuc);

	/*	printf("odrazenyLuc.x %lf\n", odrazenyLuc.x);
		printf("odrazenyLuc.y %lf\n", odrazenyLuc.y);
		printf("odrazenyLuc.z %lf\n", odrazenyLuc.z);*/

	/*	printf("normala.x: %lf\n", normala.x);
		printf("normala.y: %lf\n", normala.y);
		printf("normala.z: %lf\n", normala.z);
		bodNormaly.push_back(normala);*/

		//INICIALIZACIA VEKTORA POHLADU
		vektorPohladu.x = 0;
		vektorPohladu.y = 0;
		vektorPohladu.z = 100;
		vektorPohladu = normovanieBodu(vektorPohladu);

		/* ZRKADLOVA ZLOZKA */
		/* --------------------------------------------------------------------------------------------------------------- */
		/* Is = Il * rs * (V.N)^h */

		VkratR = (vektorPohladu.x * odrazenyLuc.x) + (vektorPohladu.y * odrazenyLuc.y) + (vektorPohladu.z * odrazenyLuc.z);
		if (VkratR <= 0) Is = QColor(0, 0, 0);
		else {
			pomRed = Il.red() * rs.redF() * pow((VkratR), ui->spinBoxOstrostZrkadlovyOdraz->value());
			if (pomRed > 255) pomRed = 255;

			pomGreen = Il.green() * rs.greenF() * pow((VkratR), ui->spinBoxOstrostZrkadlovyOdraz->value());
			if (pomGreen > 255) pomGreen = 255;

			pomBlue = Il.blue() * rs.blueF() * pow((VkratR), ui->spinBoxOstrostZrkadlovyOdraz->value());
			if (pomBlue > 255) pomBlue = 255;

			Is.setRed(pomRed);
			Is.setGreen(pomGreen);
			Is.setBlue(pomBlue);
			/*printf("Is.red je %lf\n", Is.redF());
			printf("Is.green je %lf\n", Is.greenF());
			printf("Is.blue je %lf\n", Is.blueF());*/
		}
		
		/* DIFUZNA ZLOZKA */
		/* --------------------------------------------------------------------------------------------------------------- */
		/* Id = Il * rd * (L.N) */
		LkratNnormovane = (svetelnyLucPozicia.x * normala.x) + (svetelnyLucPozicia.y * normala.y) + (svetelnyLucPozicia.z * normala.z);
		if (LkratNnormovane <= 0) Id = QColor(0, 0, 0);
		else {
			/*printf("svetelny luc %d %d %d\n", Il.red(), Il.green(), Il.blue());
			printf("rd %d %d %d\n", rd.red(), rd.green(), rd.blue());
			printf("LkratNnormoavane %lf\n", LkratNnormovane);*/
			pomRed = Il.red() * rd.redF() * LkratNnormovane;
			if (pomRed > 255) pomRed = 255;

			pomGreen = Il.green() * rd.greenF() * LkratNnormovane;
			if (pomGreen > 255) pomGreen = 255;

			pomBlue = Il.blue() * rd.blueF() * LkratNnormovane;
			if (pomBlue > 255) pomBlue = 255;

			Id.setRed(pomRed);
			Id.setGreen(pomGreen);
			Id.setBlue(pomBlue);
			
		/*	printf("\nId.red je %d\n", Id.red());
			printf("Id.green je %d\n", Id.green());
			printf("Id.blue je %d\n", Id.blue());*/
		}
	
		/* AMBIENTNA ZLOZKA */
		/* --------------------------------------------------------------------------------------------------------------- */
		/* (Ia = Io * ra) */

	//	printf("Io %d %d %d\n", Io.red(), Io.green(), Io.blue());
	//	printf("rd %d %d %d\n", ra.red(), ra.green(), ra.blue());
		pomRed = Io.red() * ra.redF();
	//	printf("pomRed %d\n", pomRed);
		if (pomRed > 255) pomRed = 255;

		pomGreen = Io.green() * ra.greenF();
	//	printf("pomGreen %d\n", pomGreen);
		if (pomGreen > 255) pomGreen = 255;

		pomBlue = Io.blue() * ra.blueF();
	//	printf("pomBlue %d\n", pomBlue);
		if (pomBlue > 255) pomBlue = 255;

		Ia.setRed(pomRed);
		Ia.setGreen(pomGreen);
		Ia.setBlue(pomBlue);
		/*printf("\nIa.red je %lf\n", Ia.redF());
		printf("Ia.green je %lf\n", Ia.greenF());
		printf("Ia.blue je %lf\n", Ia.blueF());*/

		/* SCITANIE ZLOZIEL Is, Id, Ia */
		/* --------------------------------------------------------------------------------------------------------- */

		pomRed = Is.red() + Id.red() + Ia.red();
		if (pomRed > 255) pomRed = 255;

		pomGreen = Is.green() + Id.green() + Ia.green();
		if (pomGreen > 255) pomGreen = 255;

		pomBlue = Is.blue() + Id.blue() + Ia.blue();
		if (pomBlue > 255) pomBlue = 255;

	//	printf("\nIa.red() %d\n", Ia.red());
	//	printf("Ia.green() %d\n", Ia.green());
	//	printf("Ia.blue() %d\n", Ia.blue());

		I.setRed(pomRed);
		I.setGreen(pomGreen);
		I.setBlue(pomBlue);
	//	farbaPlosky.push_back(I);
	/*	printf("--------------------rozmedzie od 0 - 1 ----------------------------\n");
		printf("\nI.redF je: %lf\n", I.redF());
		printf("I.greenF je: %lf\n", I.greenF());
		printf("I.blueF je: %lf\n", I.blueF());*/
	/*	printf("--------------------rozmedzie od 0 - 255 ----------------------------\n");
		printf("red je %d\n", I.red());
		printf("green je %d\n", I.green());
		printf("blue je %d\n", I.blue());*/
		return I;
}

void PaintWidget::osvetlitObjekt(QColor Il, QColor rs, QColor rd, QColor Io, QColor ra)
{
	QColor farba;
	if (ui->comboBoxTienovanie->currentIndex() == 0) {
		rovnobeznePremietanie();
		for (int i = 0; i < bodTrojuholnika.size(); i++) {
			bodNormaly.clear();
			pomBodTroj.clear();
			farba = PhongovOsvetlovaciModel(bodElipsy, bodTrojuholnika[i], Il, rs, rd, Io, ra);
			rozdelTrojuholnik(bodTrojuholnika[i], farba, bodRovnobezPremiet);
		}
	}
}

bool sortFunkciaTH(const QVector<float>& a, const QVector<float>& b)
{
	if (a[1] == b[1] && a[0] != b[0]) return a[0] < b[0];
	else if (a[1] == b[1] && a[0] == b[0]) return a[3] < b[3];
	else return a[1] < b[1];
}

void PaintWidget::naplnTH(BOD a, BOD b)
{
	QVector<float> hrana;
	float dy, dx, w;

	dy = a.y - b.y;
	if (a.y > b.y) {
		dx = a.x - b.x;
		w = dx / dy;
		hrana.push_back(b.x);		//Xz
		hrana.push_back(b.y);		//Yz	
		hrana.push_back(a.y - 1);	//Yk	
		hrana.push_back(w);			//w
		TH.push_back(hrana);
	}
	else if (a.y < b.y) {
		dy = b.y - a.y;
		dx = b.x - a.x;
		w = dx / dy;
		hrana.push_back(a.x);
		hrana.push_back(a.y);
		hrana.push_back(b.y - 1);
		hrana.push_back(w);
		TH.push_back(hrana);
	}
}

void PaintWidget::scanLine(QColor farba)
{
	QPainter painter(&image);
	QVector<float> priesecnikX;
	float Ya, Ymax;
	painter.setPen(QPen(QColor(farba), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	/* TH bude mat vzdy iba dve hrany */
	Ya = TH[0][1];
	Ymax = TH[1][2];

	while (Ya <= Ymax) {
		priesecnikX.clear();
		/* Xa = W*Ya - W*Yz + Xz */
		priesecnikX.push_back((TH[0][3] * Ya) - (TH[0][3] * TH[0][1]) + TH[0][0]);
		priesecnikX.push_back((TH[1][3] * Ya) - (TH[1][3] * TH[1][1]) + TH[1][0]);
		qSort(priesecnikX);
		painter.drawLine(round(priesecnikX[0]), round(Ya), round(priesecnikX[1]), round(Ya));
		Ya++;
	}
	update();
}

void PaintWidget::rozdelTrojuholnik(TROJUHOLNIK troj, QColor farba, vector<BOD> body)
{
	printf("som v ROZDEL TROJUHOLNIKY\n");
	vector<BOD> pomBod;
	pomBod.clear();
	int index0, index1, index2;
	BOD bodA, bodB, bodC, bodD, bodZ;
	float smernica, posun;
	float w, presecnikX1, priesecnikX2, Ya1, Ymax1, Ya2, Ymax2;

	bodA = body[troj.a];
	bodB = body[troj.b];
	bodC = body[troj.c];

	/* usporiadanie bodov trojuholnika vzostupne podla y suradnice, aby sme zistili bodD, ten vyratame zo stredneho bodu v usporiadanom vektore */
	if (bodA.y > bodC.y) {
		bodZ = bodA;
		bodA = bodC;
		bodC = bodZ;
	}
	if (bodA.y > bodB.y) {
		bodZ = bodA;
		bodA = bodB;
		bodB = bodZ;
	}
	if (bodB.y > bodC.y) {
		bodZ = bodB;
		bodB = bodC;
		bodC = bodZ;
	}
	/* osetrenie pre pravouhly trojuholnik */
	if (bodB.y == bodA.y) {
		TH.clear();
		naplnTH(bodB, bodC);
		naplnTH(bodA, bodC);
		qSort(TH.begin(), TH.end(), sortFunkciaTH);
		scanLine(farba);
		/* do zbufferu posielam jeden bod z trojuholnika, nezalezi na tom ktory, pretoze ak vidim jeden bod vidim ceu plosku */
		//Zbuffer(farba, bodB.z);
	}
	/* osetrenie pre pravouhly trojuholnik */
	else if (bodB.y == bodC.y) {
		TH.clear();
		naplnTH(bodA, bodB);
		naplnTH(bodA, bodC);
		qSort(TH.begin(), TH.end(), sortFunkciaTH);
		scanLine(farba);
		//Zbuffer(farba, bodB.z);
	}
	/* ak nemame pravouhly trojuholnik, potom ratame bod D */
	else {
		/* Vycislenie boduD. Jeho y suradnicu vieme. Najst xovu - pomocou analytickeho vyjadrenia y = smernica*x + posun */
		bodD.y = bodB.y;
		if ((bodC.x - bodA.x) == 0) {
			smernica = (bodC.y - bodA.y) / double(DBL_MIN + 0.0000001);
		}
		else {
			smernica = (bodC.y - bodA.y) / (bodC.x - bodA.x);
		}
		posun = bodA.y - smernica * bodA.x;
		bodD.x = (bodD.y - posun) / smernica;
		/* vypnit dolny trojuholnik */
		TH.clear();
		naplnTH(bodA, bodB);
		naplnTH(bodA, bodD);
		qSort(TH.begin(), TH.end(), sortFunkciaTH);
		scanLine(farba);
		DDA(bodB.x, bodB.y, bodD.x, bodD.y, farba);
		//Zbuffer(farba, bodB.z);

		/* vyplnit horny trojuholnik */
		TH.clear();
		naplnTH(bodB, bodC);
		naplnTH(bodD, bodC);
		qSort(TH.begin(), TH.end(), sortFunkciaTH);
		scanLine(farba);
		DDA(bodB.x, bodB.y, bodD.x, bodD.y, farba);
		//Zbuffer(farba, bodB.z);
	}
	update();
}

void PaintWidget::Zbuffer(QColor farba, float suradnicaZ)
{
	for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			F[i][j] = QColor(255, 255, 255);
			Z[i][j] = DBL_MAX;
		}
	}

	for (int i = 0; i < bodTrojuholnika.size(); i++)
	{
		for (int j = 0; j < bodElipsy.size(); j++)
		{
			if (Z[i][j] > suradnicaZ)
			{
				Z[i][j] = round(suradnicaZ);
				F[i][j] = farba;
			}
		}
	}
}

void PaintWidget::generateEllipsoid()
{
	vector<BOD> body;
	BOD bod;
	float v = 0;
	float t = 0;
	int velkost = 0;
	int a = ui->spinBoxA->value();
	int b = ui->spinBoxB->value();
	int c = ui->spinBoxC->value();
	int poludniky = ui->spinBoxPoludniky->value();
	int rovnobezky = ui->spinBoxRovnobezky->value();
	int xs = image.width() / 2;
	int ys = image.height() / 2;
	float posun_v = (2 * M_PI) / poludniky;
	float posun_t = (M_PI) / rovnobezky;
	rovnobezky++;

	fstream file;
	file.open("elipsoidVisual.vtk", fstream::out);
	file << "# vtk DataFile Version 3.0" << endl;
	file << "vtk output" << endl << "ASCII" << endl << "DATASET POLYDATA" << endl;
	file << "POINTS " << poludniky*(rovnobezky + 1) << " float" << endl;

	for (int i = 0; i <= rovnobezky; i++) {
		v = 0;
		for (int j = 0; j < poludniky; j++) {
			bod.x = a*cos(v)*sin(t);
			bod.y = b*sin(v)*sin(t);
			bod.z = c*cos(t);

			body.push_back(bod);
			velkost++;
			v += posun_v;
		}
		t += posun_t;
	}

	for (int i = 0; i < poludniky*(rovnobezky + 1); i++)
	{
		file << body[i].x << " " << body[i].y << " " << body[i].z << endl;
	}

	int pocet_poly = ((rovnobezky * poludniky) - poludniky) * 2;
	file << "POLYGONS " << pocet_poly << " " << 4 * pocet_poly << endl;
	rovnobezky--;
	for (int i = poludniky; i < poludniky * 2; i++)
	{
		if (i == (poludniky * 2) - 1) {
			file << "3 " << poludniky << " ";
		}
		else {
			file << "3 " << i + 1 << " ";
		}
		file << i << " 0" << endl;
	}

	for (int i = poludniky; i < (pocet_poly / 2); i++)
	{
		if (i % poludniky == poludniky - 1)
		{	
			i -= poludniky;
			file << "3 " << i + poludniky << " " << i + 1 << " " << i + 2 * poludniky << endl;
			file << "3 " << i + 1 << " " << i + poludniky + 1 << " " << i + 2 * poludniky << endl;
			i += poludniky;
		}
		else
		{
			file << "3 " << i << " " << i + 1 << " " << i + poludniky << endl;
			file << "3 " << i + 1 << " " << i + poludniky + 1 << " " << i + poludniky << endl;
		}
	}

	for (int i = pocet_poly / 2; i < poludniky*(rovnobezky + 1); i++)
	{
		if (i == (poludniky*(rovnobezky + 1)) - 1) {
			file << "3 " << i << " " << pocet_poly / 2 << " " << poludniky*(rovnobezky + 1) << endl;
		}
		else {
			file << "3 " << i << " " << i + 1 << " " << poludniky*(rovnobezky + 1) << endl;
		}
	}
	file.close();
	update();
}


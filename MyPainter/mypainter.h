#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "paintwidget.h"
#include <QElapsedTimer>
#include <QColorDialog>
#include <QString>
#include <QColor>
#include <stdio.h>
#include "ui_mypainter.h"
#include <qtextstream.h>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void ActionKresli();
	void ActionAzimut();
	void ActionZenit();
	void ActionGeneruj();
	void ActionSkalovat();
	void ActionPosunutie();
	void ActionSvetelnyLuc();
	void ActionKoeficientOdrazu();
	void ActionKoeficientDifuzie();
	void ActionOkoliteSvetlo();
	void ActionOsvetlit();
	void ActionKoeficientAmbientny();

private:
	Ui::MyPainterClass *ui;
	PaintWidget *paintWidget;
	QColor Il, rs, rd, Io, ra;
};

#endif // MYPAINTER_H

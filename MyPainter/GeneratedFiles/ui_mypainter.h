/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout_2;
    QPushButton *pushButtonKoeficientAmbietny;
    QFrame *line_17;
    QFrame *line_18;
    QFrame *line_20;
    QFrame *line_19;
    QPushButton *pushButtonOkoliteSvetlo;
    QFrame *line_21;
    QFrame *line_14;
    QPushButton *pushButtonSvetelnyLuc;
    QDoubleSpinBox *doubleSpinBoxOsY;
    QLabel *label_14;
    QLabel *label_4;
    QLabel *label_11;
    QDoubleSpinBox *doubleSpinBoxSkalovanie;
    QLabel *label_3;
    QSpinBox *spinBoxC;
    QLabel *label_2;
    QSpinBox *spinBoxA;
    QSpinBox *spinBoxRovnobezky;
    QLabel *label_9;
    QLabel *label_6;
    QLabel *label_15;
    QSpinBox *spinBoxB;
    QLabel *label_12;
    QLabel *label_5;
    QSpinBox *spinBoxPoludniky;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBoxOsX;
    QFrame *line_16;
    QPushButton *pushButtonSkalovat;
    QFrame *line_6;
    QLabel *label_10;
    QFrame *line_7;
    QFrame *line_8;
    QFrame *line_9;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QFrame *line_13;
    QLabel *label_7;
    QDoubleSpinBox *doubleSpinBoxVzdialenostKamery;
    QFrame *line_10;
    QPushButton *pushButtonKresli;
    QFrame *line_11;
    QFrame *line_2;
    QPushButton *pushButtonKoeficientOdrazu;
    QPushButton *pushButtonKoeficientDifuzie;
    QFrame *line;
    QFrame *line_12;
    QFrame *line_3;
    QFrame *line_4;
    QFrame *line_15;
    QFrame *line_22;
    QComboBox *comboBoxPremietanie;
    QLabel *label_8;
    QFrame *line_23;
    QFrame *line_5;
    QFrame *line_24;
    QFrame *line_25;
    QFrame *line_26;
    QFrame *line_27;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBoxOsZ;
    QLabel *label_16;
    QSpinBox *spinBoxOstrostZrkadlovyOdraz;
    QLabel *label_17;
    QComboBox *comboBoxTienovanie;
    QPushButton *pushButton_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QSlider *horizontalSliderAzimut;
    QSlider *horizontalSliderZenit;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(613, 636);
        MyPainterClass->setStyleSheet(QLatin1String("selection-background-color: rgb(255, 255, 255);\n"
"background-color: rgb(218, 255, 212);\n"
"gridline-color: rgb(115, 255, 28);\n"
"selection-color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(255, 255, 255);\n"
"border-color: rgb(171, 255, 114);\n"
"font: 8pt \"Palatino Linotype\";\n"
"selection-color: rgb(255, 255, 255);"));
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, -1, 10, 10);
        pushButtonKoeficientAmbietny = new QPushButton(centralWidget);
        pushButtonKoeficientAmbietny->setObjectName(QStringLiteral("pushButtonKoeficientAmbietny"));
        pushButtonKoeficientAmbietny->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonKoeficientAmbietny, 1, 4, 1, 1);

        line_17 = new QFrame(centralWidget);
        line_17->setObjectName(QStringLiteral("line_17"));
        line_17->setFrameShape(QFrame::VLine);
        line_17->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_17, 16, 2, 1, 1);

        line_18 = new QFrame(centralWidget);
        line_18->setObjectName(QStringLiteral("line_18"));
        line_18->setFrameShape(QFrame::VLine);
        line_18->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_18, 17, 2, 1, 1);

        line_20 = new QFrame(centralWidget);
        line_20->setObjectName(QStringLiteral("line_20"));
        line_20->setFrameShape(QFrame::VLine);
        line_20->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_20, 19, 2, 1, 1);

        line_19 = new QFrame(centralWidget);
        line_19->setObjectName(QStringLiteral("line_19"));
        line_19->setFrameShape(QFrame::VLine);
        line_19->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_19, 18, 2, 1, 1);

        pushButtonOkoliteSvetlo = new QPushButton(centralWidget);
        pushButtonOkoliteSvetlo->setObjectName(QStringLiteral("pushButtonOkoliteSvetlo"));
        pushButtonOkoliteSvetlo->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonOkoliteSvetlo, 3, 4, 1, 1);

        line_21 = new QFrame(centralWidget);
        line_21->setObjectName(QStringLiteral("line_21"));
        line_21->setFrameShape(QFrame::VLine);
        line_21->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_21, 20, 2, 1, 1);

        line_14 = new QFrame(centralWidget);
        line_14->setObjectName(QStringLiteral("line_14"));
        line_14->setFrameShape(QFrame::VLine);
        line_14->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_14, 13, 2, 1, 1);

        pushButtonSvetelnyLuc = new QPushButton(centralWidget);
        pushButtonSvetelnyLuc->setObjectName(QStringLiteral("pushButtonSvetelnyLuc"));
        pushButtonSvetelnyLuc->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonSvetelnyLuc, 2, 4, 1, 1);

        doubleSpinBoxOsY = new QDoubleSpinBox(centralWidget);
        doubleSpinBoxOsY->setObjectName(QStringLiteral("doubleSpinBoxOsY"));
        doubleSpinBoxOsY->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        doubleSpinBoxOsY->setMaximum(800);

        gridLayout_2->addWidget(doubleSpinBoxOsY, 17, 1, 1, 1);

        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_2->addWidget(label_14, 0, 3, 1, 1);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 3, 0, 1, 1);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_2->addWidget(label_11, 16, 0, 1, 1);

        doubleSpinBoxSkalovanie = new QDoubleSpinBox(centralWidget);
        doubleSpinBoxSkalovanie->setObjectName(QStringLiteral("doubleSpinBoxSkalovanie"));
        doubleSpinBoxSkalovanie->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(doubleSpinBoxSkalovanie, 12, 1, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        spinBoxC = new QSpinBox(centralWidget);
        spinBoxC->setObjectName(QStringLiteral("spinBoxC"));
        spinBoxC->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        spinBoxC->setMaximum(800);

        gridLayout_2->addWidget(spinBoxC, 5, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        spinBoxA = new QSpinBox(centralWidget);
        spinBoxA->setObjectName(QStringLiteral("spinBoxA"));
        spinBoxA->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        spinBoxA->setMaximum(800);

        gridLayout_2->addWidget(spinBoxA, 3, 1, 1, 1);

        spinBoxRovnobezky = new QSpinBox(centralWidget);
        spinBoxRovnobezky->setObjectName(QStringLiteral("spinBoxRovnobezky"));
        spinBoxRovnobezky->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(spinBoxRovnobezky, 1, 1, 1, 1);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_2->addWidget(label_9, 12, 0, 1, 1);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 5, 0, 1, 1);

        label_15 = new QLabel(centralWidget);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_2->addWidget(label_15, 1, 3, 1, 1);

        spinBoxB = new QSpinBox(centralWidget);
        spinBoxB->setObjectName(QStringLiteral("spinBoxB"));
        spinBoxB->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        spinBoxB->setMaximum(800);

        gridLayout_2->addWidget(spinBoxB, 4, 1, 1, 1);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_2->addWidget(label_12, 17, 0, 1, 1);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 4, 0, 1, 1);

        spinBoxPoludniky = new QSpinBox(centralWidget);
        spinBoxPoludniky->setObjectName(QStringLiteral("spinBoxPoludniky"));
        spinBoxPoludniky->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(spinBoxPoludniky, 0, 1, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        doubleSpinBoxOsX = new QDoubleSpinBox(centralWidget);
        doubleSpinBoxOsX->setObjectName(QStringLiteral("doubleSpinBoxOsX"));
        doubleSpinBoxOsX->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        doubleSpinBoxOsX->setMaximum(800);

        gridLayout_2->addWidget(doubleSpinBoxOsX, 16, 1, 1, 1);

        line_16 = new QFrame(centralWidget);
        line_16->setObjectName(QStringLiteral("line_16"));
        line_16->setFrameShape(QFrame::VLine);
        line_16->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_16, 15, 2, 1, 1);

        pushButtonSkalovat = new QPushButton(centralWidget);
        pushButtonSkalovat->setObjectName(QStringLiteral("pushButtonSkalovat"));
        pushButtonSkalovat->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonSkalovat, 13, 1, 1, 1);

        line_6 = new QFrame(centralWidget);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setFrameShape(QFrame::VLine);
        line_6->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_6, 5, 2, 1, 1);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_2->addWidget(label_10, 15, 0, 1, 1);

        line_7 = new QFrame(centralWidget);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setFrameShape(QFrame::VLine);
        line_7->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_7, 6, 2, 1, 1);

        line_8 = new QFrame(centralWidget);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setFrameShape(QFrame::VLine);
        line_8->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_8, 7, 2, 1, 1);

        line_9 = new QFrame(centralWidget);
        line_9->setObjectName(QStringLiteral("line_9"));
        line_9->setFrameShape(QFrame::VLine);
        line_9->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_9, 8, 2, 1, 1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButton_2, 20, 1, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButton, 7, 1, 1, 1);

        line_13 = new QFrame(centralWidget);
        line_13->setObjectName(QStringLiteral("line_13"));
        line_13->setFrameShape(QFrame::VLine);
        line_13->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_13, 12, 2, 1, 1);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_2->addWidget(label_7, 13, 3, 1, 1);

        doubleSpinBoxVzdialenostKamery = new QDoubleSpinBox(centralWidget);
        doubleSpinBoxVzdialenostKamery->setObjectName(QStringLiteral("doubleSpinBoxVzdialenostKamery"));
        doubleSpinBoxVzdialenostKamery->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        doubleSpinBoxVzdialenostKamery->setValue(1);

        gridLayout_2->addWidget(doubleSpinBoxVzdialenostKamery, 12, 4, 1, 1);

        line_10 = new QFrame(centralWidget);
        line_10->setObjectName(QStringLiteral("line_10"));
        line_10->setFrameShape(QFrame::VLine);
        line_10->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_10, 9, 2, 1, 1);

        pushButtonKresli = new QPushButton(centralWidget);
        pushButtonKresli->setObjectName(QStringLiteral("pushButtonKresli"));
        pushButtonKresli->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonKresli, 20, 4, 1, 1);

        line_11 = new QFrame(centralWidget);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setFrameShape(QFrame::VLine);
        line_11->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_11, 10, 2, 1, 1);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_2, 2, 2, 1, 1);

        pushButtonKoeficientOdrazu = new QPushButton(centralWidget);
        pushButtonKoeficientOdrazu->setObjectName(QStringLiteral("pushButtonKoeficientOdrazu"));
        pushButtonKoeficientOdrazu->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonKoeficientOdrazu, 4, 4, 1, 1);

        pushButtonKoeficientDifuzie = new QPushButton(centralWidget);
        pushButtonKoeficientDifuzie->setObjectName(QStringLiteral("pushButtonKoeficientDifuzie"));
        pushButtonKoeficientDifuzie->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButtonKoeficientDifuzie, 5, 4, 1, 1);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 3, 2, 1, 1);

        line_12 = new QFrame(centralWidget);
        line_12->setObjectName(QStringLiteral("line_12"));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_12, 11, 4, 1, 1);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_3, 1, 2, 1, 1);

        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_4, 0, 2, 1, 1);

        line_15 = new QFrame(centralWidget);
        line_15->setObjectName(QStringLiteral("line_15"));
        line_15->setFrameShape(QFrame::HLine);
        line_15->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_15, 11, 3, 1, 1);

        line_22 = new QFrame(centralWidget);
        line_22->setObjectName(QStringLiteral("line_22"));
        line_22->setFrameShape(QFrame::HLine);
        line_22->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_22, 11, 1, 1, 1);

        comboBoxPremietanie = new QComboBox(centralWidget);
        comboBoxPremietanie->setObjectName(QStringLiteral("comboBoxPremietanie"));
        comboBoxPremietanie->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(comboBoxPremietanie, 13, 4, 1, 1);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_2->addWidget(label_8, 12, 3, 1, 1);

        line_23 = new QFrame(centralWidget);
        line_23->setObjectName(QStringLiteral("line_23"));
        line_23->setFrameShape(QFrame::HLine);
        line_23->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_23, 11, 0, 1, 1);

        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::VLine);
        line_5->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_5, 4, 2, 1, 1);

        line_24 = new QFrame(centralWidget);
        line_24->setObjectName(QStringLiteral("line_24"));
        line_24->setFrameShape(QFrame::HLine);
        line_24->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_24, 14, 0, 1, 1);

        line_25 = new QFrame(centralWidget);
        line_25->setObjectName(QStringLiteral("line_25"));
        line_25->setFrameShape(QFrame::HLine);
        line_25->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_25, 14, 1, 1, 1);

        line_26 = new QFrame(centralWidget);
        line_26->setObjectName(QStringLiteral("line_26"));
        line_26->setFrameShape(QFrame::HLine);
        line_26->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_26, 14, 3, 1, 1);

        line_27 = new QFrame(centralWidget);
        line_27->setObjectName(QStringLiteral("line_27"));
        line_27->setFrameShape(QFrame::HLine);
        line_27->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_27, 14, 4, 1, 1);

        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_2->addWidget(label_13, 18, 0, 1, 1);

        doubleSpinBoxOsZ = new QDoubleSpinBox(centralWidget);
        doubleSpinBoxOsZ->setObjectName(QStringLiteral("doubleSpinBoxOsZ"));
        doubleSpinBoxOsZ->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        doubleSpinBoxOsZ->setMaximum(800);

        gridLayout_2->addWidget(doubleSpinBoxOsZ, 18, 1, 1, 1);

        label_16 = new QLabel(centralWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_2->addWidget(label_16, 6, 3, 1, 1);

        spinBoxOstrostZrkadlovyOdraz = new QSpinBox(centralWidget);
        spinBoxOstrostZrkadlovyOdraz->setObjectName(QStringLiteral("spinBoxOstrostZrkadlovyOdraz"));
        spinBoxOstrostZrkadlovyOdraz->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        spinBoxOstrostZrkadlovyOdraz->setMinimum(1);

        gridLayout_2->addWidget(spinBoxOstrostZrkadlovyOdraz, 6, 4, 1, 1);

        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_2->addWidget(label_17, 7, 3, 1, 1);

        comboBoxTienovanie = new QComboBox(centralWidget);
        comboBoxTienovanie->setObjectName(QStringLiteral("comboBoxTienovanie"));
        comboBoxTienovanie->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(comboBoxTienovanie, 7, 4, 1, 1);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));

        gridLayout_2->addWidget(pushButton_3, 9, 4, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 164, 515));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout->addWidget(scrollArea);


        verticalLayout->addLayout(horizontalLayout);

        horizontalSliderAzimut = new QSlider(centralWidget);
        horizontalSliderAzimut->setObjectName(QStringLiteral("horizontalSliderAzimut"));
        horizontalSliderAzimut->setStyleSheet(QStringLiteral(""));
        horizontalSliderAzimut->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSliderAzimut);

        horizontalSliderZenit = new QSlider(centralWidget);
        horizontalSliderZenit->setObjectName(QStringLiteral("horizontalSliderZenit"));
        horizontalSliderZenit->setStyleSheet(QStringLiteral(""));
        horizontalSliderZenit->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(horizontalSliderZenit);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 613, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionClear, SIGNAL(triggered()), MyPainterClass, SLOT(EffectClear()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(pushButtonKresli, SIGNAL(clicked()), MyPainterClass, SLOT(ActionKresli()));
        QObject::connect(horizontalSliderAzimut, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(ActionAzimut()));
        QObject::connect(horizontalSliderZenit, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(ActionZenit()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(ActionGeneruj()));
        QObject::connect(pushButtonSkalovat, SIGNAL(clicked()), MyPainterClass, SLOT(ActionSkalovat()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(ActionPosunutie()));
        QObject::connect(pushButtonSvetelnyLuc, SIGNAL(clicked()), MyPainterClass, SLOT(ActionSvetelnyLuc()));
        QObject::connect(pushButtonKoeficientOdrazu, SIGNAL(clicked()), MyPainterClass, SLOT(ActionKoeficientOdrazu()));
        QObject::connect(pushButtonKoeficientDifuzie, SIGNAL(clicked()), MyPainterClass, SLOT(ActionKoeficientDifuzie()));
        QObject::connect(pushButtonOkoliteSvetlo, SIGNAL(clicked()), MyPainterClass, SLOT(ActionOkoliteSvetlo()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(ActionOsvetlit()));
        QObject::connect(pushButtonKoeficientAmbietny, SIGNAL(clicked()), MyPainterClass, SLOT(ActionKoeficientAmbientny()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", 0));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", 0));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", 0));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", 0));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", 0));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", 0));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", 0));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", 0));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", 0));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", 0));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", 0));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", 0));
        pushButtonKoeficientAmbietny->setText(QApplication::translate("MyPainterClass", "Koeficient ambientny", 0));
        pushButtonOkoliteSvetlo->setText(QApplication::translate("MyPainterClass", "Okolite svetlo", 0));
        pushButtonSvetelnyLuc->setText(QApplication::translate("MyPainterClass", "Svetelny luc", 0));
        label_14->setText(QApplication::translate("MyPainterClass", "Phongov O.M.l", 0));
        label_4->setText(QApplication::translate("MyPainterClass", "    A", 0));
        label_11->setText(QApplication::translate("MyPainterClass", "v smere osi x", 0));
        label_3->setText(QApplication::translate("MyPainterClass", "Parametre", 0));
        label_2->setText(QApplication::translate("MyPainterClass", "Pocet rovnobeziek", 0));
        label_9->setText(QApplication::translate("MyPainterClass", "Skalovanie", 0));
        label_6->setText(QApplication::translate("MyPainterClass", "    C", 0));
        label_15->setText(QApplication::translate("MyPainterClass", "Vybrat farbu pre", 0));
        label_12->setText(QApplication::translate("MyPainterClass", "v smere osi y", 0));
        label_5->setText(QApplication::translate("MyPainterClass", "    B", 0));
        label->setText(QApplication::translate("MyPainterClass", "Pocet poludnikov", 0));
        pushButtonSkalovat->setText(QApplication::translate("MyPainterClass", "Skalovat", 0));
        label_10->setText(QApplication::translate("MyPainterClass", "Posunutie", 0));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "Posunut", 0));
        pushButton->setText(QApplication::translate("MyPainterClass", "Generovat", 0));
        label_7->setText(QApplication::translate("MyPainterClass", "Premietanie", 0));
        doubleSpinBoxVzdialenostKamery->setPrefix(QString());
        doubleSpinBoxVzdialenostKamery->setSuffix(QString());
        pushButtonKresli->setText(QApplication::translate("MyPainterClass", "Kreslit", 0));
        pushButtonKoeficientOdrazu->setText(QApplication::translate("MyPainterClass", "Koeficient odrazu", 0));
        pushButtonKoeficientDifuzie->setText(QApplication::translate("MyPainterClass", "Koeficient difuzie", 0));
        comboBoxPremietanie->clear();
        comboBoxPremietanie->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "Rovnobezne", 0)
         << QApplication::translate("MyPainterClass", "Stredove ", 0)
        );
        label_8->setText(QApplication::translate("MyPainterClass", "Vzdialenost kamery", 0));
        label_13->setText(QApplication::translate("MyPainterClass", "v smere osi z", 0));
        label_16->setText(QApplication::translate("MyPainterClass", "Ostrost ZO", 0));
        label_17->setText(QApplication::translate("MyPainterClass", "Tienovanie", 0));
        comboBoxTienovanie->clear();
        comboBoxTienovanie->insertItems(0, QStringList()
         << QApplication::translate("MyPainterClass", "Konstantne", 0)
         << QApplication::translate("MyPainterClass", "Gouraudovo", 0)
        );
        pushButton_3->setText(QApplication::translate("MyPainterClass", "Osvetlit", 0));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", 0));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", 0));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H

#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <vector>
#include "ui_mypainter.h"
#include <fstream>
#include <QTextStream>
#include <QColorDialog>
using namespace std;

typedef struct {
	float x, y, z;
} BOD;

typedef struct {
	int a, b, c;
}TROJUHOLNIK;

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(Ui::MyPainterClass *parentUi, QWidget *parent = 0);
	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }
	void DDA(int x1, int y1, int x2, int y2, QColor farba);
	void generateEllipsoid();
	void spracovanieSuboru(QTextStream *text);
	void rovnobeznePremietanie();
	void stredovePremietanie();
	void rotaciaOkoloZ(QColor Il, QColor rs, QColor rd, QColor Io, QColor ra);
	void rotaciaOkoloX(QColor Il, QColor rs, QColor rd, QColor Io, QColor ra);
	vector<BOD> vykreslenie(vector<BOD> upraveneBody);
	void posunutie();
	void skalovanie();
	QColor PhongovOsvetlovaciModel(vector<BOD> body, TROJUHOLNIK troj, QColor Il, QColor rs, QColor rd, QColor Io, QColor ra);
	void osvetlitObjekt(QColor Il, QColor rs, QColor rd, QColor Io, QColor ra);
	void rozdelTrojuholnik(TROJUHOLNIK troj, QColor farba, vector<BOD> body);
	void Zbuffer(QColor farba, float suradnicaZ);
	void scanLine(QColor farba);
	void naplnTH(BOD a, BOD b);
	BOD normovanieBodu(BOD bod);

public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

private:
	Ui::MyPainterClass *ui;
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
	vector<TROJUHOLNIK> bodTrojuholnika;
	vector<BOD> bodElipsy;
	vector<BOD> bodUpraveny;
	vector<BOD> bodNormaly;
	vector<QColor> farbaPlosky;
	QVector<QVector<float>> TH;
	vector<BOD> pomBodTroj;
	vector<BOD> bodRovnobezPremiet;
	vector<BOD> bodStredovyPriemet;
	vector<vector<QColor>> F;
	vector<vector<double>> Z;
	int velkost;
	int xs, ys, pocetBodov;
	vector<BOD> bodZrotovanyPosunuty;
};

#endif // PAINTWIDGET_H

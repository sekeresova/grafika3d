#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MyPainterClass)
{
	paintWidget = new PaintWidget(ui);
	ui->setupUi(this);
	ui->scrollArea->setWidget(this->paintWidget);
	ui->scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget->newImage(800, 800);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString nazovSuboru = QFileDialog::getOpenFileName(this, "Otvorit subor", "", "vtk (*.vtk)");
	QFile data(nazovSuboru);

	if (!data.open(QIODevice::ReadOnly)) {
		return;
	}
	QTextStream text(&data);
	paintWidget->spracovanieSuboru(&text);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget->saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget->clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget->newImage(800, 800);
}

void MyPainter::ActionKresli()
{
	if (ui->comboBoxPremietanie->currentIndex() == 0) {
		paintWidget->rovnobeznePremietanie();
	}
	if (ui->comboBoxPremietanie->currentIndex() == 1) {
		paintWidget->stredovePremietanie();
	}
}

void MyPainter::ActionAzimut()
{
	EffectClear();
	paintWidget->rotaciaOkoloX(Il, rs, rd, Io, ra);
}

void MyPainter::ActionZenit()
{
	EffectClear();
	paintWidget->rotaciaOkoloZ(Il, rs, rd, Io, ra);
}

void MyPainter::ActionGeneruj()
{
	paintWidget->generateEllipsoid();
}

void MyPainter::ActionSkalovat()
{
	EffectClear();
	paintWidget->skalovanie();
}

void MyPainter::ActionPosunutie()
{
	EffectClear();
	paintWidget->posunutie();
}

void MyPainter::ActionSvetelnyLuc()
{
	QColorDialog colorDialog;
	Il = colorDialog.getColor();
}

void MyPainter::ActionKoeficientOdrazu()
{
	QColorDialog colorDialog;
	rs = colorDialog.getColor();
}

void MyPainter::ActionKoeficientDifuzie()
{
	QColorDialog colorDialog;
	rd = colorDialog.getColor();
}

void MyPainter::ActionOkoliteSvetlo()
{
	QColorDialog colorDialog;
	Io = colorDialog.getColor();
}

void MyPainter::ActionKoeficientAmbientny()
{
	QColorDialog colorDialog;
	ra = colorDialog.getColor();
}

void MyPainter::ActionOsvetlit()
{
	paintWidget->osvetlitObjekt(Il, rs, rd, Io, ra);
}